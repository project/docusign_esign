<?php

namespace Drupal\docusign_esign;

use DocuSign\eSign\Api\EnvelopesApi;
use DocuSign\eSign\Model\Document;
use Drupal\Core\File\FileSystemInterface;
use Drupal\docusign_esign\Exception\DocusignException;

/**
 * Class Document Manager to upload and download files with docusign.
 */
class DocumentManager {

  /**
   * Create the document array for docusign envelope definition.
   *
   * @param string $document_uri
   *   Uri where the application pdf to be signed, is stored.
   * @param string $filename
   *   Filename of document being created.
   *
   * @return \DocuSign\eSign\Model\Document[]
   *   Document array for envelope definition.
   */
  public function createDocument(string $document_uri, string $filename): array {
    $content_bytes = file_get_contents($document_uri);
    $doc1_b64 = base64_encode($content_bytes);

    return [
      new Document([
        'document_base64' => $doc1_b64,
        'name' => $filename,
        'file_extension' => 'pdf',
        'document_id' => '1',
      ]),
    ];
  }

  /**
   * Create the documents array with multiple docs for envelope definition.
   *
   * @param array $documentInfo
   *   Array of file uris and filenames to create documents.
   *
   * @return \DocuSign\eSign\Model\Document[]
   *   Documents array for envelope definition
   *
   * @throws \Drupal\docusign_esign\Exception\DocusignException
   */
  public function createDocuments(array $documentInfo): array {
    $documents = [];
    $index = 1;
    foreach ($documentInfo as $doc) {
      if (isset($doc['file_uri']) && isset($doc['filename'])) {
        try {
          $content_bytes = file_get_contents($doc['file_uri']);
          $doc1_b64 = base64_encode($content_bytes);

          $documents[] = new Document([
            'document_base64' => $doc1_b64,
            'name' => $doc['filename'],
            'file_extension' => 'pdf',
            'document_id' => (string) $index,
          ]);
        }
        catch (\Exception $e) {
          throw new DocusignException($e);
        }
      }
    }

    return $documents;

  }

  /**
   * Downloads signed document and returns filename/file uri of signed PDF.
   *
   * @param \DocuSign\eSign\Api\EnvelopesApi $envelopeApi
   *   Envelope api to call.
   * @param string $accountId
   *   Account ID for use when calling the envelope API.
   * @param string $envelopeId
   *   Envelope ID to retrieve envelope.
   * @param string $documentId
   *   Document ID in Envelope of application PDF.
   *
   * @return string
   *   File name of downloaded document.
   *
   * @throws \DocuSign\eSign\Client\ApiException
   */
  public function downloadDocument(EnvelopesApi $envelopeApi, string $accountId, string $envelopeId, string $documentId): string {
    $file = $envelopeApi->getDocument($accountId, $documentId, $envelopeId);

    $fileSystem = \Drupal::service('file_system');
    $directory = 'private://signed';
    $fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);
    $fileUri = 'private://signed/temp_signed.pdf';
    file_put_contents($fileUri, file_get_contents($file->getPathname()));

    return $fileUri;
  }

}
