<?php

namespace Drupal\docusign_esign\Client;

use DocuSign\eSign\Client\ApiClient;
use Drupal\docusign_esign\Auth\DefaultAccount;
use Drupal\docusign_esign\Auth\JWTService;

/**
 * Class to initialize the Docusign Client.
 */
class Client {

  /**
   * The JWT Authentication Service.
   *
   * @var \Drupal\docusign_esign\Auth\JWTService
   */
  private JWTService $jwtService;

  /**
   * The access token obtained from the connection.
   *
   * @var string
   */
  private string $accessToken;

  /**
   * The Docusign Api Client.
   *
   * @var \DocuSign\eSign\Client\ApiClient
   */
  private ApiClient $client;

  /**
   * The Default Account associated with the User.
   *
   * @var \Drupal\docusign_esign\Auth\DefaultAccount
   */
  private DefaultAccount $account;

  /**
   * Constructs a new Client Object.
   *
   * @param \Drupal\docusign_esign\Auth\JWTService $jwtService
   *   The JWT Authentication Service.
   */
  public function __construct(JWTService $jwtService) {
    $this->jwtService = $jwtService;
    $this->account = $this->jwtService->getAccount();
    $this->accessToken = $this->jwtService->getOAuthToken()->getAccessToken();
    $this->initClient();
  }

  /**
   * Helper to initialize the Docusign API Client.
   */
  private function initClient(): void {
    $client = $this->jwtService->getClient();
    $clientConfig = $client->getConfig();
    $clientConfig->setHost($this->account->getBaseUri() . '/restapi');
    $clientConfig->addDefaultHeader('Authorization', 'Bearer ' . $this->accessToken);
    $this->client = $client;
  }

  /**
   * Get the Account ID.
   */
  public function getAccountId(): string {
    return $this->account->getId();
  }

  /**
   * Get the ApiClient.
   */
  public function getClient(): ApiClient {
    return $this->client;
  }

  /**
   * Get the default account from the connection.
   */
  public function getAccount(): DefaultAccount {
    return $this->account;
  }

}
