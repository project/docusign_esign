<?php

namespace Drupal\docusign_esign\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Northern Docusign Integration settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'docusign_esign_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['docusign_esign.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['apps_and_keys'] = [
      '#type' => 'fieldset',
      '#title' => 'Apps And Integration Keys',
    ];

    $form['apps_and_keys']['integration_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Integration Key'),
      '#default_value' => $this->config('docusign_esign.settings')
        ->get('integration_key'),
      '#required' => TRUE,
    ];

    $form['account_info'] = [
      '#type' => 'fieldset',
      '#title' => 'My Account Information',
    ];

    $form['account_info']['user_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User ID'),
      '#default_value' => $this->config('docusign_esign.settings')
        ->get('user_id'),
      '#required' => TRUE,
    ];

    $form['oauth'] = [
      '#type' => 'fieldset',
      '#title' => 'OAuth Information',
    ];

    $form['oauth']['oauth_base_path'] = [
      '#type' => 'radios',
      '#title' => $this->t('Server Type'),
      '#options' => [
        'account-d.docusign.com' => $this->t('Demo'),
        'account.docusign.com' => $this->t('Production'),
      ],
      '#default_value' => $this->config('docusign_esign.settings')
        ->get('oauth_base_path'),
    ];

    $form['test_connection'] = [
      '#type' => 'submit',
      '#value' => 'Test Docusign API Connection',
      '#submit' => [[$this, 'testConnection']],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Submit Handler; Tests connectivity to Docusign API.
   */
  public function testConnection(array &$form, FormStateInterface $form_state) {
    $this->config('docusign_esign.settings')
      ->set('integration_key', $form_state->getValue('integration_key'))
      ->set('user_id', $form_state->getValue('user_id'))
      ->set('oauth_base_path', $form_state->getValue('oauth_base_path'))
      ->save();

    $success = FALSE;
    try {
      $success = \Drupal::service('docusign_esign.jwt')->ping();
    }
    catch (\Exception $e) {
      $this->logger('Docusign Connection Test')->error($e);
    }

    if ($success) {
      $this->messenger()
        ->addStatus('Successfully connected to the Docusign API');
    }
    else {
      $this->messenger()
        ->addError('Connection to Docusign API Failed');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!$this->config('docusign_esign.app_token')
      ->get('application_token')) {
      $this->messenger()
        ->addWarning($this->t('Please Consent for this application after filling out the values in this configuration form. To consent go to /docusign-esign/consent'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('docusign_esign.settings')
      ->set('integration_key', $form_state->getValue('integration_key'))
      ->set('user_id', $form_state->getValue('user_id'))
      ->set('oauth_base_path', $form_state->getValue('oauth_base_path'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
