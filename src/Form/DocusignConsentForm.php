<?php

namespace Drupal\docusign_esign\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\docusign_esign\Auth\ApplicationConsent;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Consent application for docusign.
 */
class DocusignConsentForm extends FormBase {

  /**
   * Injected Docusign Application Consent Service.
   *
   * @var \Drupal\docusign_esign\Auth\ApplicationConsent
   */
  private ApplicationConsent $consentService;

  /**
   * Constructor for UpdateSignaturesForm.
   *
   * @var \Drupal\docusign_esign\Auth\ApplicationConsent $docusignConsentService
   */
  public function __construct(ApplicationConsent $docusignConsentService) {
    $this->consentService = $docusignConsentService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('docusign_esign.consent')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'docusign_esign_consent_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Consent for environment.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->consentService->consent();
  }

}
