<?php

namespace Drupal\docusign_esign;

use DocuSign\eSign\Api\EnvelopesApi;
use DocuSign\eSign\Client\ApiException;
use DocuSign\eSign\Model\Envelope;
use DocuSign\eSign\Model\EnvelopeDefinition;
use DocuSign\eSign\Model\RecipientEmailNotification;
use DocuSign\eSign\Model\Recipients;
use DocuSign\eSign\Model\Signer;
use DocuSign\eSign\Model\SignHere;
use DocuSign\eSign\Model\Tabs;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\docusign_esign\Auth\JWTService;
use Drupal\docusign_esign\Client\Client;
use Drupal\docusign_esign\Exception\DocusignException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Docusign client connection service.
 */
class DocusignApi {

  /**
   * Docusign Api Client Object.
   *
   * @var \DocuSign\eSign\Client\ApiClient
   */
  private $client;

  /**
   * Docusign Envelopes Api Object.
   *
   * @var \DocuSign\eSign\Api\EnvelopesApi
   */
  private $envelopes;

  /**
   * Docusign Account ID for connection.
   *
   * @var string
   */
  private $accountId;

  /**
   * Logger Service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private $logger;

  /**
   * Messenger Service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  private $messenger;

  /**
   * Constructor to create the Docusign API object.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   Logger factory for DocusignApi.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   Messenger Service.
   * @param \Drupal\docusign_esign\Auth\JWTService $jwtService
   *   JWT Service.
   */
  public function __construct(LoggerChannelFactoryInterface $loggerFactory, Messenger $messenger, JWTService $jwtService) {
    $this->logger = $loggerFactory->get('DocusignApi');
    $this->messenger = $messenger;

    $docusignClient = new Client($jwtService);
    $this->accountId = $docusignClient->getAccountId();
    $this->client = $docusignClient->getClient();
    $this->envelopes = new EnvelopesApi($this->client);
  }

  /**
   * Instantiates a new instance of the DocusignApi Class.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('docusign_esign.jwt')
    );
  }

  /**
   * Call api and get envelope object from envelope ID.
   *
   * @param string $envelope_id
   *   ID of envelope to retrieve.
   *
   * @return \DocuSign\eSign\Model\Envelope
   *   Envelope.
   *
   * @throws \Drupal\docusign_esign\Exception\DocusignException
   */
  public function getEnvelope(string $envelope_id): Envelope {
    try {
      return $this->envelopes->getEnvelope($this->accountId, $envelope_id);
    }
    catch (ApiException $e) {
      $this->logger->error(print_r($e->getMessage(), TRUE));
      $this->messenger->addError('Something went wrong with retrieving docusign envelope.');
      throw new DocusignException($e);
    }
  }

  /**
   * Creates and sends envelopes to be signed.
   *
   * This method works if you have a pdf that you need signed, stored in your
   * files. If you call this method with the Document URI and configure the
   * makeEnvelope, createRecipients, and createTabs methods you will be able to
   * send an envelope to be signed.
   *
   * @param string $documentUri
   *   Uri of stored application PDF to be signed.
   *
   * @return string|null
   *   ID of envelope.
   */
  public function createAndSendEnvelope(string $documentUri): ?string {
    try {
      $envelope = $this->makeEnvelope('test email', $documentUri);
      $envelopeResponse = $this->envelopes->createEnvelope($this->accountId, $envelope);
      $this->logger->info('Created and sent Docusign Envelope. Envelope ID: ' . $envelopeResponse->getEnvelopeId());
      return $envelopeResponse->getEnvelopeId();
    }
    catch (ApiException $e) {
      $this->logger->error(print_r($e->getMessage(), TRUE));
      throw new DocusignException($e);
    }
  }

  /**
   * Makes/Creates an Envelope.
   *
   * @param string $emailSubject
   *   Email Subject Line.
   * @param string $documentUri
   *   String Document Uri.
   *
   * @return \DocuSign\eSign\Model\EnvelopeDefinition
   *   Envelope definition to be sent to api to create an envelope.
   */
  public function makeEnvelope(string $emailSubject, string $documentUri): EnvelopeDefinition {
    $envelopeDefinition = new EnvelopeDefinition();
    $envelopeDefinition->setEmailSubject($emailSubject);
    $envelopeDefinition->setDocuments((new DocumentManager())->createDocument($documentUri, 'temp_filename'));
    $envelopeDefinition->setRecipients($this->createRecipients('test test', 'email@test.com', $emailSubject, 'test email body'));
    $envelopeDefinition->setStatus('sent');

    return $envelopeDefinition;
  }

  /**
   * Create recipients object for envelope definition.
   *
   * @return \DocuSign\eSign\Model\Recipients
   *   Recipient object for envelope definition.
   */
  public function createRecipients($name, $email, $emailSubject, $emailBody): Recipients {
    $signers = [
      new Signer([
        'name' => $name,
        'email' => $email,
        'routing_order' => '1',
        'status' => 'created',
        'delivery_method' => 'Email',
        'recipient_id' => "1",
        'tabs' => $this->createTabs(),
        'email_notification' => new RecipientEmailNotification([
          'email_subject' => $emailSubject,
          'email_body' => $emailBody,
        ]),
      ]),
    ];

    $recipients = new Recipients();
    $recipients->setSigners($signers);

    return $recipients;
  }

  /**
   * Example: Create Tabs for recipient creation.
   *
   * @return \DocuSign\eSign\Model\Tabs
   *   Tabs object for use in recipient creation.
   */
  public function createTabs(): Tabs {
    $tabs = new Tabs();
    $tab = new SignHere();
    $tab->setAnchorString('Signature');
    $tab->setAnchorXOffset('0');
    $tab->setAnchorYOffset('40');
    $tab->setAnchorUnits('pixels');
    $tab->setTooltip('Please Sign Here');
    $tabs->setSignHereTabs([$tab]);

    return $tabs;
  }

  /**
   * Get status of the envelope.
   *
   * @param string $envelopeId
   *   ID of envelope to retrieve status from.
   *
   * @return string|null
   *   Status of envelope.
   *
   * @throws \Drupal\docusign_esign\Exception\DocusignException
   */
  public function getEnvelopeStatus(string $envelopeId): ?string {
    $envelopeResponse = $this->getEnvelope($envelopeId);
    return $envelopeResponse->getStatus();
  }

  /**
   * Download the signed document from a completed envelope.
   *
   * @param string $envelopeId
   *   ID of completed envelope.
   * @param string $documentId
   *   ID of document to download from envelope.
   *
   * @return string
   *   Location of stored signed application pdf.
   *
   * @throws \DocuSign\eSign\Client\ApiException
   */
  public function downloadSignedDocument(string $envelopeId, string $documentId = '1'): string {
    $documentManager = new DocumentManager();
    return $documentManager->downloadDocument($this->envelopes, $this->accountId, $envelopeId, $documentId);
  }

}
