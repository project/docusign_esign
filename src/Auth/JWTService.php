<?php

namespace Drupal\docusign_esign\Auth;

use DocuSign\eSign\Client\ApiClient;
use DocuSign\eSign\Client\Auth\OAuthToken;
use DocuSign\eSign\Configuration;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\docusign_esign\Exception\DocusignException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Docusign JWT token authentication service.
 */
class JWTService implements ContainerInjectionInterface {

  /**
   * Config id.
   */
  public const CONFIG_NAME = 'docusign_esign.settings';

  /**
   * Docusign Api Client.
   *
   * @var \DocuSign\eSign\Client\ApiClient
   */
  private ApiClient $client;

  /**
   * The config from 'docusign_esign.settings'.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected Config $config;

  /**
   * OAuth Token for Authentication.
   *
   * @var string
   */
  private mixed $oAuthToken;

  /**
   * Docusign Api Account Info.
   *
   * @var \Drupal\docusign_esign\Auth\DefaultAccount
   */
  private DefaultAccount $account;

  /**
   * Logger Channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private LoggerChannelInterface $logger;

  /**
   * Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  private MessengerInterface $messenger;

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a new JwtAuthentication object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger factory.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   Drupal messenger.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity Type Manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\docusign_esign\Exception\DocusignException
   * @throws \DocuSign\eSign\Client\ApiException
   */
  public function __construct(ConfigFactoryInterface $configFactory, LoggerChannelFactoryInterface $loggerFactory, Messenger $messenger, EntityTypeManagerInterface $entityTypeManager) {
    $this->config = $configFactory->getEditable(self::CONFIG_NAME);
    $this->logger = $loggerFactory->get('DocusignJWTAuthenticationService');
    $this->messenger = $messenger;
    $this->entityTypeManager = $entityTypeManager;

    $this->client = new ApiClient(new Configuration());
    $this->oAuthToken = $this->authorizationFlow();
    $this->account = $this->createAccount();
  }

  /**
   * Instantiates a new JWT Service.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Test the connection to the Docusign API.
   *
   * @return bool
   *   True if connection to Docusign API was successful.
   *
   * @throws \DocuSign\eSign\Client\ApiException
   */
  public function ping(): bool {
    return ($this->client->getUserInfo($this->oAuthToken->getAccessToken())[1] === 200);
  }

  /**
   * Creates a default account.
   *
   * @return \Drupal\docusign_esign\Auth\DefaultAccount
   *   The DefaultAccount Object.
   *
   * @throws \DocuSign\eSign\Client\ApiException
   */
  private function createAccount(): DefaultAccount {
    $account = new DefaultAccount();
    $userInfo = $this->client->getUserInfo($this->oAuthToken->getAccessToken())[0];
    $accounts = $userInfo['accounts'];

    foreach ($accounts as $acc) {
      if ($acc->getIsDefault()) {
        $accountInfo = $acc;
      }
    }

    if (!isset($accountInfo)) {
      $accountInfo = $accounts[0];
    }

    $account->setId($accountInfo->getAccountId());
    $account->setBaseUri($accountInfo->getBaseUri());

    return $account;
  }

  /**
   * Get the Default Account from the Connection Settings.
   */
  public function getAccount(): DefaultAccount {
    return $this->account;
  }

  /**
   * Get the ApiClient.
   */
  public function getClient(): ApiClient {
    return $this->client;
  }

  /**
   * Get The OAuth Token.
   */
  public function getOAuthToken(): OAuthToken {
    return $this->oAuthToken;
  }

  /**
   * Authenticate docusign api using JWT token authentication flow.
   *
   * @return mixed|void
   *   Returns OAuthToken Object from Authentication response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\docusign_esign\Exception\DocusignException
   */
  public function authorizationFlow() {
    $this->client->getOAuth()
      ->setOAuthBasePath($this->config->get('oauth_base_path'));

    $privateKey = $this->entityTypeManager
      ->getStorage('key')
      ->load('docusign_private_key')
      ->getKeyValue();

    try {
      $response = $this->getClient()->requestJWTUserToken(
        $this->config->get('integration_key'),
        $this->config->get('user_id'),
        $privateKey,
        'signature impersonation'
      );

      return $response[0];
    }
    catch (\Throwable $th) {
      if (strpos($th->getMessage(), "consent_required") !== FALSE) {
        $this->messenger->addWarning('Application Needs Consent');
        $message = 'Application Needs Consent';
      }
      else {
        $this->logger->error($th->getMessage());
        $this->messenger->addError($th->getMessage());
        $message = 'Unknown Error Check Logs';
      }
      throw new DocusignException($message);
    }
  }

}
