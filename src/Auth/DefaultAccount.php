<?php

namespace Drupal\docusign_esign\Auth;

/**
 * Model for a new Default Account.
 */
class DefaultAccount {

  /**
   * The Account ID.
   *
   * @var string
   */
  private string $id;

  /**
   * The Account Base Uri.
   *
   * @var string
   */
  private string $baseUri;

  /**
   * Construct a Default Account.
   *
   * @param string $id
   *   Account ID.
   * @param string $baseUri
   *   Account Base Uri.
   */
  public function __construct(string $id = '', string $baseUri = '') {
    $this->id = $id;
    $this->baseUri = $baseUri;
  }

  /**
   * Get the Account ID.
   */
  public function getId(): string {
    return $this->id;
  }

  /**
   * Set the Account ID.
   *
   * @param string $id
   *   The Account ID.
   */
  public function setId(string $id): void {
    $this->id = $id;
  }

  /**
   * Get the Account Base uri.
   */
  public function getBaseUri(): string {
    return $this->baseUri;
  }

  /**
   * Set the Account Base Uri.
   *
   * @param string $baseUri
   *   The Account Base Uri.
   */
  public function setBaseUri(string $baseUri): void {
    $this->baseUri = $baseUri;
  }

}
