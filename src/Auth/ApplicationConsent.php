<?php

namespace Drupal\docusign_esign\Auth;

use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class Docusign Application Consent service.
 */
class ApplicationConsent {

  /**
   * Config id.
   */
  public const CONFIG_NAME = 'docusign_esign.settings';

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Constructs a new Application Consent object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->config = $configFactory->getEditable(self::CONFIG_NAME);
  }

  /**
   * Instantiates a new instance of the ApplicationConsent Class.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * Docusign application consent.
   *
   * Gets docusign consent for the current application environment.
   * Only needs to be executed once per environment as the token does not
   * expire.
   */
  public function consent() {
    global $base_url;
    $auth_uri = $this->createAuthorizationUri(
      $this->config->get('oauth_base_path'),
      $this->config->get('integration_key'),
      'signature impersonation',
      $base_url . '/docusign-application-token/',
      'code');
    $redirect = new RedirectResponse($auth_uri);
    $redirect->send();
  }

  /**
   * Helper for docusign application consent method.
   *
   * Creates authorization uri for the redirection to consent.
   *
   * @param string $oauth_base_path
   *   Oauth Base Path for api connection.
   * @param string $client_id
   *   Integration key for api.
   * @param string $scopes
   *   Api scope list. Space seperated.
   * @param string $redirect_uri
   *   Redirect Url to return too.
   * @param string $response_type
   *   Api response type.
   *
   * @return string
   *   Authorization URI string.
   */
  private function createAuthorizationUri(string $oauth_base_path, string $client_id, string $scopes, string $redirect_uri, string $response_type): string {
    return 'https://' . $oauth_base_path .
      '/oauth/auth?response_type=' . $response_type .
      '&scope=' . $scopes .
      '&client_id=' . $client_id .
      '&redirect_uri=' . $redirect_uri;
  }

}
