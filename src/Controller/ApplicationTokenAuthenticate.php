<?php

namespace Drupal\docusign_esign\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Http\RequestStack;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\Messenger;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class ApplicationTokenAuthenticate authenticate the environment.
 *
 * @package Drupal\docusign_esign\Controller
 */
class ApplicationTokenAuthenticate extends ControllerBase {

  /**
   * Logger Service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private $logger;

  /**
   * Messenger Service.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  public $messenger;

  /**
   * Current HTTP Request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  private $request;

  /**
   * Editable Northern Docusign Settings Config.
   *
   * @var \Drupal\Core\Config\Config
   */
  private $config;

  /**
   * Constructor for UpdateSignaturesService.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   LoggerFactory.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   Drupal Messenger Service.
   * @param \Drupal\Core\Http\RequestStack $requestStack
   *   Request Stack.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config Factory.
   */
  public function __construct(LoggerChannelFactoryInterface $loggerFactory, Messenger $messenger, RequestStack $requestStack, ConfigFactoryInterface $configFactory) {
    $this->logger = $loggerFactory->get('Docusign Application Consent');
    $this->messenger = $messenger;
    $this->request = $requestStack->getCurrentRequest();
    $this->config = $configFactory->getEditable('docusign_esign.app_token');
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('request_stack'),
      $container->get('config.factory')
    );
  }

  /**
   * Authenticate this application environment.
   */
  public function authenticate(): RedirectResponse {
    $app_token = $this->parseUrlQueries($this->request->getRequestUri());

    if ($app_token) {
      $this->config->set('application_token', $app_token)->save();
      $this->messenger->addStatus('Successfully gave consent to create Docusign envelopes for application.');
      $this->logger->info('Successfully gave consent to create Docusign envelopes for application.');
    }
    else {
      $this->messenger->addError('ERROR: Docusign Environment Authentication Failed');
      $this->logger->error('ERROR: Docusign Environment Authentication Failed');
    }

    global $base_secure_url;
    $redirect = new RedirectResponse($base_secure_url . '/admin/config/docusign-esign');
    $redirect->send();
    return $redirect;
  }

  /**
   * Helper to parse url params.
   *
   * @param string $url
   *   Url containing the query params to be parsed.
   *
   * @return string
   *   Application Authentication Token
   */
  private function parseUrlQueries(string $url): string {
    $url_arr = parse_url($url);
    isset($url_arr['query']) && $queries = explode('&', $url_arr['query']);

    $app_token = NULL;
    if (isset($queries)) {
      foreach ($queries as $query) {
        $query = explode('=', $query);
        if ($query[0] == 'code' && isset($query[1])) {
          $app_token = $query[1];
        }
      }
    }

    return $app_token;
  }

}
