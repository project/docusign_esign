<?php

namespace Drupal\docusign_esign\Exception;

/**
 * Handle Docusign errors.
 */
class DocusignException extends \Exception {}
