CONTENTS OF THIS FILE
---------------------

- Introduction
- Requirements
- Recommended Modules
- Installation
- Configuration
  - Consent and Configuration
  - Configuration Settings
  - Generating RSA Key pairs
  - Docusign Redirect Uri
  - Consenting
  - Testing the Connection
- Maintainers

INTRODUCTION
------------
This module is a skeleton for using the DocuSign eSignature API to send and manage envelopes and more.
The authentication type it uses is the JWT Grant and the module uses impersonation of one account to perform
all actions.

Out of the box this module handles all Authentication.
After setting up by following the Installation and Configuration sections you
will be able to test the api connection and add any functionality needed within the DocusignApi.php class in the module.
Some test/demo methods are already implemented in DocusignApi.php and need minimal configuration to send a first test
envelope.

Some features that are included out of the box and can be used:

- **Document manager:**
  This Document Manager class can download pdfs from the file uri and a filename you would like the document to have in
  the envelopeand return a \DocuSign\eSign\Model\Document[] to use in envelope creation. Can create a single document or
  multiple documents by using a different method that takes an array of file_uris and filenames. Can also use this class
  to download a document after it has been signed from DocuSign. This new document will contain the signatures etc...

For detailed info on the creation of Envelopes, tabs, recipients, etc... you can view
the [Docusign eSignature API Documentation](https://developers.docusign.com/docs/esign-rest-api/reference/)

REQUIREMENTS
------------
This module requires the following modules:

- [Key](https://www.drupal.org/project/key)

This module requires a developer account with docusign to work with the
[DocuSign eSignature API](https://developers.docusign.com/docs/esign-rest-api/)

The eSignature api implementation in this module requires the library:

- docusign/esign-client:^6.9

RECOMMENDED MODULES
-------------------

- [Markdown filter](https://www.drupal.org/project/markdown):
  When enabled, display of the project's README.md help will be rendered
  with markdown.

INSTALLATION
------------
To add this to your custom modules clone the repo into you custom modules folder:

```
cd path/to/modules/custom
git clone <path-to-src-code>
```

This module requires the docusign php sdk for the Docusign eSignature API and
also requires the Key module from drupal.

Install the eSignature api package using composer:

```
composer require docusign/esign-client:^6.9
```

Install the drupal/key module with composer:

```
composer require drupal/key
```

CONFIGURATION
-------------

Every api call made from the module to docusign needs authentication. For each environment before the module can be used
there are a few steps to obtain that authentication.

### Docusign Consent and Configuration (Manual Configuration Required)

On each new environment you need to get Docusign's consent for that application. This only needs
to be obtained once per environment and produces an **Application Token** that will be saved in config
and used to generate **JWT Tokens** which will then be used to generate access tokens for api calls.

Before obtaining consent the configuration settings must be added to `/admin/config/docusign-esign/settings`

### Step 1: Configuration Settings

To locate the values to input in the configuration settings log into https://developers.docusign.com/
then in the *my account* dropdown navigate to `My Apps & Keys` (also found under settings)

##### Config Values at `/admin/config/docusign-esign/settings`:

`Integration Key` = found under **Apps and Integration Keys**

`User ID` = found under **My Account Information**

`OAuth Base Path` =

1. if using the Production Docusign server: `Prod`
2. if using the Demo Docusign server: `Demo`

### Step 2: Generating RSA Key Pairs

On the **My Apps and Keys** Page on the docusign site find the integration key you are using
and select edit. This will let you edit the integration key settings.

1. Ensure in **Authorization/User Application** that `Authorization Code Grant` is selected.
2. In **Authentication/Service Integration** select/generate the rsa key pair with `+ Generate RSA`

* Save these two keys in the drupal server in a `keys/` folder inside the **private files** folder
* RSA Key Pairs needed for connection through JWT Tokens to Docusign are located in `private_files/keys` directory
* Name the files `docusign_private_key.key` and `docusign_public_key.key`.

3. Add the keys into the `drupal/key` module

* Add the keys at `/admin/config/system/keys/add`
* **Key name** = `docusign_private_key` and `docusign_public_key` respectively.
* **Key provider** setting = `File`
* **File Location** = wherever the rsa key pair files were saved.
  * Usually `private://keys/docusign_private_key.key` and `private://keys/docusign_public_key.key`
* **Description** can be left blank

Make sure the RSA Key files are saved somewhere because they cannot be accessed again on docusign.

Also make sure you `save` everything on the edit page of the integration key to save this RSA Key Pair.
The save button is found at the bottom of this page.

### Step 3: Add the redirect URI for obtaining consent

Go to edit the integration key which is the same page as the previous step.

In the **Additional Settings** section add a redirect uri that will be used to obtain consent for the
application and store the `application_token` code in the **docusign_esign** config which will be used to
authenticate all api calls.

The Redirect URI to add:

```
https://[SITE_BASE_URL]/docusign-application-token/
```

example redirect uri: `https://example.ca/docusign-application-token/`

**This redirect uri needs to be a secure protocol (https://).**

Also make sure you `save` everything on the edit page of the integration key to save this Redirect URI.
The save button is found at the bottom of this page.

### Step 4: Consenting

After all the previous steps have been completed, of the settings page click save configuration.
It should show a warning saying

Please Consent for this application after filling out the values in this configuration form. To consent go to
`/docusign-esign/consent`

Consent with the button: `Consent`

Three possible things may happen.

1. If you are not logged into docusign. It will take you to a login page.
   There you will input the same info you used to log into the developer account. It will then
   prompt you to `consent for this application` and select consent. You will be redirected to the drupal site.
   This will consent for this environment and store the application token for use in authentication.

2. If you are logged into docusign but have not consented yet. It will just prompt
   you to consent. Select `consent`. You will be redirected back to the drupal site.
   This will consent for this environment and store the application token for use in authentication.

3. If you are logged into docusign and have previously consented. You will get redirected back to the drupal site.

After any of these three situations if the application consent was successfully obtained
a status message will show on the drupal site saying:

```
Successfully gave consent to create Docusign envelopes for application.
```

and a will also log a message saying the same thing. If the consenting failed
a message saying consent failed will show and be logged.

If for any reason no status messages show after attempting to consent. Check logs.
Their may have been an error and if no error and successful consent the success
message will be logged.

### Step 5: Test Connection

After successfully obtaining consent. Test the connection of the application.

Go to `/admin/config/docusign-esign/settings` and use the `Test Connection`
button. If successful a success message will display otherwise an error message will display.

_**If connection was successful Docusign Connection has been completed and the API is ready to be
used.**_

MAINTAINERS
-----------

- [Lucas Fraulin](https://www.drupal.org/user/3680190)
